const path = require('path');
const Zip = require('zip-webpack-plugin');

const debug = process.env.NODE_ENV === 'local';

const timeStamp = new Date().toISOString().replace(/-|T|:|\.(.*)Z/g, '');

module.exports = {
  context: path.join(__dirname, 'src'),
  devtool: debug ? 'inline-sourcemap' : '',
  entry: {
    'greenfare-lambda': './lambda/index',
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.json?$/,
        use: 'json-loader',
        exclude: /node_modules/,
      },
    ],
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].js',
    library: process.env.npm_package_name,
    libraryTarget: 'commonjs2',
    umdNamedDefine: true,
  },
  plugins: [
    new Zip({ filename: `${process.env.npm_package_name}-${process.env.npm_package_version}-${timeStamp}.zip` }),
  ],
};
