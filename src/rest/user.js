import express from 'express';
import mongoose from 'mongoose';
import passport from 'passport';


import {
  ensureSecureToken,
  ensureAuthenticated,
  sendSecureToken,
} from '../common/route-util';

export default function () {
  const router = express.Router();
  const Account = mongoose.models.Account;

  router.post('/signup', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
      return res.status(400).send({
        status: 'ERROR',
        service: 'user',
        message: 'invalid form',
      });
    }
    const acct = new Account({ email });
    return Account.register(acct, password, (err) => {
      if (err) {
        return res.status(500).send({ status: 'ERROR', service: 'user', message: err.message });
      }
      return res.status(201).send({ status: 'OK', service: 'user' });
    });
  });

  router.post('/auth', passport.authenticate('local'), sendSecureToken);

  router.get('/', ensureSecureToken(), ensureAuthenticated, (req, res) => {
    console.log('-------', req.secure);
    res.status(200).send({ doh: 'doh' });
  });

  return router;
}
