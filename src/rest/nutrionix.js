import express from 'express';
import fetch from 'node-fetch';
import log from '../common/log';

import {
  ensureSecureToken,
  ensureAuthenticated,
} from '../common/route-util';

export default function () {
  const { NUTRIONIX_URL, NUTRIONIX_API_ID, NUTRIONIX_API_KEY } = process.env;
  const router = express.Router();

  router.get('/:id', ensureSecureToken(), ensureAuthenticated, async (req, res) => {
    const url = `${NUTRIONIX_URL}?appId=${NUTRIONIX_API_ID}&appKey=${NUTRIONIX_API_KEY}&upc=${req.params.id}`;
    log.info(req.user.id, req.user.email, url);
    const response = await fetch(url);
    const data = await response.json();
    res.send(data);
  });
  return router;
}
