import jwt from 'jsonwebtoken';
import passport from 'passport';

export function ensureSSL(req, res, next) {
  const env = process.env;
  if (req.app.locals.httpsServer && req.secure && req.protocol === 'https') {
    next();
  } else if (!req.app.locals.httpsServer && req.get('X-Forwarded-Proto') === 'https') {
    next();
  } else {
    res.redirect(`https://${env.WEB_HOST}:${env.WEB_SSL_PORT}${req.originalUrl}`);
  }
}

export function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(403).send({});
  }
}

export function sendSecureToken(req, res) {
  const token = jwt.sign({ id: req.user.id }, process.env.SECRET, {});
  res.send({ token });
}

export function ensureSecureToken() {
  return passport.authenticate('bearer', { session: false });
}
