import dateFormat from 'dateformat';

export function colorStatus(req, res) {
  // get the status code if response written
  const status = res._header ? res.statusCode : undefined;

  let color = 0; // no color
  if (status >= 500) {
    color = 31; // red
  } else if (status >= 400) {
    color = 33; // yellow
  } else if (status >= 300) {
    color = 36; // cyan
  } else if (status >= 200) {
    color = 32; // green
  }

  return `\x1b[${color}m${status}\x1b[0m`;
}

export function localDateISO() {
  const date = new Date();
  return dateFormat(date, 'isoDateTime');
}
