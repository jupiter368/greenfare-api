import tracer from 'tracer';

const level = process.env.LOG_LEVEL || 'log';
const logger = (process.env.NODE_ENV !== 'production')
  ? tracer.colorConsole({ level, dateformat: 'isoDateTime' })
  : tracer.console({ level, dateformat: 'isoDateTime' });

export default logger;
