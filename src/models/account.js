import mongoose from 'mongoose';
import passportStrategy from 'passport-local-mongoose';

const Schema = mongoose.Schema;

const accountSchema = new Schema({
  email: { type: String, required: true, index: true, unique: true },
  created: { type: Date, default: Date.now },
  updated: { type: Date },
  confirmed: { type: Boolean, default: false },
}, { safe: true });

accountSchema.plugin(passportStrategy, { usernameField: 'email' });

export default mongoose.model('Account', accountSchema);
