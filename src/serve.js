import express from 'express';
import morgan from 'morgan';
import errorHandler from 'errorhandler';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import passport from 'passport';
import mongoose from 'mongoose';
import bearer from 'passport-http-bearer';
import jwt from 'jsonwebtoken';
import cors from 'cors';

import log from './common/log';
import * as config from './config/env';
import httpInit from './config/http-init';
import dbInit from './config/db-init';
import routeInit from './config/route-init';
import { colorStatus, localDateISO } from './common/morgan-tokens';


const env = config[process.env.NODE_ENV || 'development'];
Object.keys(env).forEach((key) => {
  if (!process.env[key]) {
    process.env[key] = env[key];
  }
});

// setup custom morgan tokens
morgan.token('color-status', colorStatus);
morgan.token('local-date-iso', localDateISO);

const run = async () => {
  const app = express();

  app.use(morgan(':local-date-iso :method :url :color-status :response-time ms - :res[content-length]'));
  app.use(errorHandler({
    dumpExceptions: true,
    showStack: true,
  }));

  await dbInit();
  const Account = mongoose.models.Account;
  passport.use(Account.createStrategy()); // passport-local-mongoose local strategy
  passport.use(new bearer.Strategy(async (token, done) => { // bearer strategy pass
    try {
      const meta = jwt.verify(token, process.env.SECRET);
      const one = await Account.findById(meta.id).exec();
      if (!one) {
        return done(null, false);
      }
      return done(null, one, { scope: 'all' });
    } catch (err) {
      log.debug(err);
      return done(err, null);
    }
  }));
  passport.serializeUser(Account.serializeUser());
  passport.deserializeUser(Account.deserializeUser());
  app.set('trust proxy', 1); // behind a proxy 1 hop

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.raw());
  app.use(passport.initialize()); // authentication strategy needs this
  app.use(cookieParser(process.env.SECRET));
  app.use(cors());

  routeInit(app);
  httpInit(app);
};

run();
