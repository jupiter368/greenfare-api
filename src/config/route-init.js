import { ensureSSL } from '../common/route-util';
import userRoute from '../rest/user';
import nutrionixRoute from '../rest/nutrionix';

export default function (app) {
  app.use('/api/user', userRoute());
  app.use('/api/nutritionix', nutrionixRoute());
}
