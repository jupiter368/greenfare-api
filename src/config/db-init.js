import mongoose from 'mongoose';
import path from 'path';
import fs from 'fs';
import log from '../common/log';

const mongoOptions = { useMongoClient: true };

mongoose.Promise = Promise;

export default async function () {
  const env = process.env;
  const port = (env.DB_PORT.length > 0) ? `:${env.DB_PORT}` : '';
  const login = (env.DB_USER.length > 0) ? `${env.DB_USER}:${env.DB_PW}@` : '';
  const dbURL = `mongodb://${login}${env.DB_HOST}${port}/${env.DB_NAME}`;

  mongoose.connect(dbURL, mongoOptions).then(() => {
    log.info(`Successfully connected to: ${dbURL}`);
  }, (err) => {
    log.error(`ERROR connecting to: ${dbURL} ${err}`);
  });

  const modelsPath = path.resolve(`${__dirname}/../models`);
  fs.readdirSync(modelsPath).forEach((file) => {
    const lib = `${modelsPath}/${file}`;
    log.debug(lib);
    // eslint-disable-line global-require
    require(lib);
  });
}
