import fs from 'fs';
import http from 'http';
import https from 'https';

import log from '../common/log';

export default function (app) {
  const app2 = app;

  // for testing only
  const httpServer = http.createServer(app2);
  httpServer.listen(3000, () => {
    log.info('HTTP: Express server listening on port %d in %s mode', httpServer.address().port, app2.get('env'));
  });


  const key = fs.readFileSync(`${__dirname}/../../resources/key.pem`, 'utf8');
  const cert = fs.readFileSync(`${__dirname}/../../resources/server.crt`, 'utf8');
  const httpsServer = https.createServer({ key, cert }, app2);
  httpsServer.listen(process.env.WEB_SSL_PORT, () => {
    log.info('HTTPS: Express server listening on port %d in %s mode', httpsServer.address().port, app2.get('env'));
  });
  app2.locals.httpsServer = httpsServer;
}
