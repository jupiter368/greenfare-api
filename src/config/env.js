export const development = {
  DB_NAME: 'devDB',
  DB_HOST: 'localhost',
  DB_USER: '',
  DB_PW: '',
  DB_PORT: 27017,
  WEB_HOST: 'localhost',
  WEB_PORT: 3000,
  WEB_SSL_PORT: 5000,

  SECRET: 'S3CRE7',
  LOG_LEVEL: 'debug',

  NUTRIONIX_API_ID: 'ce7a5f5c',
  NUTRIONIX_API_KEY: '4df0e8f48741111153679d04f217c976',
  NUTRIONIX_URL: 'https://api.nutritionix.com/v1_1/item',
};

export const qa = {
  DB_NAME: 'devQA',
  DB_HOST: 'localhost',
  DB_USER: '',
  DB_PW: '',
  DB_PORT: 27017,
  WEB_HOST: 'localhost',
  WEB_PORT: 3001,
  WEB_SSL_PORT: 5001,

  SECRET: 'S3E2',
  LOG_LEVEL: 'info',

  NUTRIONIX_API_ID: 'ce7a5f5c',
  NUTRIONIX_API_KEY: '4df0e8f48741111153679d04f217c976',
};

export const production = {
  WEB_PORT: 80,
  WEB_SSL_PORT: 443,
  LOG_LEVEL: 'info',
};
