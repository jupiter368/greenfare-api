#!/usr/bin/env node
const aws = require('aws-sdk');
const fs = require('fs');
const path = require('path');

const region = 'us-east-1';
const filePrefix = `${process.env.npm_package_name}-${process.env.npm_package_version}`;

function findZipFile() {
  const files = fs.readdirSync('./build');
  const rx = new RegExp(`${filePrefix}(.*).zip`);
  let lastFile;
  files.forEach((file) => {
    if (rx.test(file)) {
      lastFile = file;
    }
  });
  return lastFile;
}

const ZIP_FILE = findZipFile();
if (!ZIP_FILE) {
  console.error('ERROR: unable to find zip file in build folder');
  process.exit(1);
}

const tpVars = {
  ZIP_FILE,
  APP_NAME: 'greenfare-api',
  NODE_ENV: process.env.NODE_ENV || 'qa',
  LOG_LEVEL: 'trace',
};

const StackName = `GF-${tpVars.APP_NAME}-${tpVars.NODE_ENV}`;

if (process.argv.length < 3) {
  console.error('ERROR: require arguments');
  process.exit(1);
}

const inputs = {};
for (let i = 2; i < process.argv.length; i++) {
  const arg = process.argv[i].split('=');
  if (arg[0].length > 0 && arg[1].length > 0) {
    inputs[arg[0]] = arg[1];
  }
}

async function uploadLambda() {
  const s3 = new aws.S3({ region });
  return new Promise((resolve, reject) => {
    s3.upload({
      Bucket: `${tpVars.APP_NAME}-${tpVars.NODE_ENV}`,
      Key: `lambda/${tpVars.ZIP_FILE}`,
      ContentType: 'application/zip',
      ServerSideEncryption: 'AES256',
      StorageClass: 'REDUCED_REDUNDANCY',
      Body: fs.createReadStream(`./build/${tpVars.ZIP_FILE}`),
    }, (err, data) => {
      const message = (err) ? err.message : data.Location;
      console.log('===== S3 UPLOAD:', message);
      if (err) { reject(err); } else { resolve(data); }
    });
  });
}

async function deployStack() {
  let TemplateBody = fs.readFileSync(path.join(__dirname, 'cloud-formation.yml'), 'UTF8');
  Object.keys(tpVars).forEach((key) => {
    const rx = new RegExp(`\\$\\{${key}\\}`, 'g');
    TemplateBody = TemplateBody.replace(rx, tpVars[key]);
  });

  const cloudFormation = new aws.CloudFormation({ region });
  const params = {
    StackName,
    TemplateBody,
    Capabilities: ['CAPABILITY_IAM'],
  };

  return new Promise((resolve, reject) => {
    cloudFormation.describeStacks({ StackName }, (err, data) => {
      let message = (err) ? err.message : data.Stacks[0].StackId;
      console.log('===== Cloud Formation DESCRIBE:', message);
      if (err) {
        cloudFormation.createStack(params, (err1, data1) => {
          message = (err1) ? err1.message : data1.StackId;
          console.log('===== Cloud Formation CREATE:', message);
          if (err1) { reject(err1); } else { resolve(data1); }
        });
      } else {
        cloudFormation.updateStack(params, (err2, data2) => {
          message = (err2) ? err2.message : data2.StackId;
          console.log('===== Cloud Formation UPDATE:', message);
          if (err2) { reject(err2); } else { resolve(data2); }
        });
      }
    });
  });
}

async function deleteStack() {
  return new Promise((resolve, reject) => {
    const cloudFormation = new aws.CloudFormation({ region });
    cloudFormation.deleteStack({ StackName }, (err, data) => {
      const message = (err) ? err.message : data;
      console.log('===== Cloud Formation DELETE:', message);
      if (err) { reject(err); } else { resolve(data); }
    });
  });
}

async function run() {
  if (inputs.action === 'stage') {
    await uploadLambda();
  }
  if (inputs.action === 'deploy') {
    await deployStack();
  }
  if (inputs.action === 'delete') {
    await deleteStack();
  }
  console.log('END!!!');
}

run().catch(() => {});
